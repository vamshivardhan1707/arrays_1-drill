function reduce(elements, cb, startingValue) {
    if (Array.isArray(elements)) {
        let result;

        if (startingValue === undefined) {
            result = elements[0];
            for (let index = 1; index < elements.length; index++) {
                result = cb(result, elements[index], index, elements);
            }
        } else {
            result = startingValue;
            for (let index = 0; index < elements.length; index++) {
                result = cb(result, elements[index], index, elements);
            };
        };

        return result;
    } else {
        return [];
    };
};

module.exports = reduce;



