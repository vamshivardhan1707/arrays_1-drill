function find(elements, cb) {
    if (Array.isArray(elements)) {

        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index])) {
                return elements[index];
            };
        };
        return undefined; 
    } else {
        return [];
    };
};

function cb(element) {
    if (element > 4) {
        return true;
    } else {
        return false; 
    };
};

module.exports = { find, cb };
