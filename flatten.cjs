function flatten(elements, depth = 1) {
    if (Array.isArray(elements)) {
        let newArray = [];
        let current = 0;

        function recursive(arr, currentDepth) {
            for (let index = 0; index < arr.length; index++) {
                if (Array.isArray(arr[index]) && currentDepth < depth) {
                    recursive(arr[index], currentDepth + 1);
                } else if (arr[index] != undefined) {
                    newArray.push(arr[index]);
                };
            };
        };

        recursive(elements, current);
        return newArray;
    } else {
        return [];
    };
};

module.exports = flatten;