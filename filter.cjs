function filter(elements, cb) {
    if (Array.isArray(elements)) {
        let newArray = []; 

        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements) == true) {
                newArray.push(elements[index]);
            };
        };
        return newArray; 
    } else {
        return [];
    };
};

module.exports = filter;
