const elements = [1, 2, 3, 4, 5, 5];
const map = require('../map.cjs'); 

function cb(value, index, elements) {
    if (typeof(value) === 'number') {
        return value * value;
    } else {
        return "Not a number";
    };
};

console.log(map(elements, cb)); 
